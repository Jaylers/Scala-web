package controllers.manager

import java.sql.Timestamp

import controllers.components.PostComponent
import models.Post

import scala.concurrent.Await
import scala.concurrent.duration.Duration

object PostManager {

  import controllers.connection.PostgresDbProvider.db
  def postNow(post : Post): Int ={
    Await.result(db.run(PostComponent.insert(post)), Duration.Inf)
  }

  def getAllPostWithoutMe(email:String): List[(String, Timestamp, String, String, String, String, String, String)] = {
    Await.result(db.run(PostComponent.getAllPostWithoutMe(email)), Duration.Inf)
  }

  def getMyPost(email:String):List[Post] = {
    Await.result(db.run(PostComponent.searchByUsers(email)), Duration.Inf)
  }

  def getThisFriendsPost(email:String):List[Post] = {
    Await.result(db.run(PostComponent.searchByUsers(email)),Duration.Inf)
  }
}
